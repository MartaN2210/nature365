process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../app');

const should = chai.should();

chai.use(chaiHttp);

describe('Stories', () => {
  describe('/GET stories', () => {
    it('it should GET all the stories', (done) => {
      chai
        .request(server)
        .get('/stories')
        .end((err, res) => {
          res.should.have.status(200);
          res.body.should.be.a('array');
          done();
        });
    });
  });
});

describe('/POST story', () => {
  it('it should post the story info', (done) => {
    const story = {
      title: 'Dark Moon',
      excerpt: 'The story is about ...',
      fileName: 'image123.jp',
    };
    chai
      .request(server)
      .post('/stories')
      .send(story)
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('object');
        res.body.should.have.property('message');
        done();
      });
  });
});

describe('/PATCH/:id story', () => {
  it('should update the story info', (done) => {
    const story = {
      title: 'Dark Moon',
      excerpt: 'The story is about ...',
      fileName: 'image123.jp',
    };
    const idStory = 17;
    chai
      .request(server)
      .patch(`/stories/${idStory}`)
      .send(story)
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('object');
        res.body.should.have.property('message');
        done();
      });
  });
});

describe('/DELETE/:id story', () => {
  it('should delete the story info', (done) => {
    const idStory = 1234;
    chai
      .request(server)
      .delete(`/stories/${idStory}`)
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a('object');
        res.body.should.have.property('data');
        done();
      });
  });
});
