const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');
const connection = require('./db_connection');

function initialize(passport, getUserByEmail) {
  const authenticateUser = async (email, password, done) => {
    const user = await getUserByEmail(email);
    if (!user) {
      return done(null, false);
    }
    try {
      if (await bcrypt.compare(password, user.password)) {
        return done(null, user);
      }
      return done(null, false);
    } catch (e) {
      return done(e);
    }
  };

  passport.use(new LocalStrategy({ usernameField: 'email' }, authenticateUser));
  passport.serializeUser((user, done) => {
    done(null, user.idUser);
  });
  passport.deserializeUser(async (id, done) => {
    try {
      const str = `SELECT * FROM users WHERE idUser=?`;
      connection.query(str, [id], (err, rows) => {
        const user = rows[0];
        console.log(user);
        return done(null, user);
      });
    } catch (err) {
      done(err);
    }
  });
}

module.exports = initialize;
