const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
// app.set('trust proxy', true);
// app.use(cors());
app.use(
  cors({
    origin: [
      'http://localhost:3000',
      'http://localhost:5000',
      'https://martan2210.gitlab.io',
    ],
    credentials: true,
  })
);

// app.use((req, res, next) => {
//   res.header('Access-Control-Allow-Origin', '*');
//   res.header('Access-Control-Allow-Credentials', true);
//   res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
//   res.header(
//     'Access-Control-Allow-Headers',
//     'Origin,X-Requested-With,Content-Type,Accept,content-type,application/json'
//   );
//   next();
// });

require('dotenv').config();

const session = require('express-session');

const server = require('http').createServer(app);
const connection = require('./db_connection');

const sessionMiddleware = session({
  secret: process.env.SESSION_SECRET,
  resave: false,
  saveUninitialized: false,
  cookie: {
    httpOnly: true,
    maxAge: 8 * 60 * 60 * 1000, // 8 hours
  },
});
app.use(sessionMiddleware);

const authRoutes = require('./routes/auth');
const daysRoutes = require('./routes/days');
const storiesRoutes = require('./routes/stories');
const storyPagesRoutes = require('./routes/storyPages');
const favouriteStoriesRoutes = require('./routes/favouriteStories');
const accomplishedActivities = require('./routes/accomplishedActivities');
const gainedRewards = require('./routes/gainedRewards');
const activitiesRoutes = require('./routes/activities');
const usersRoutes = require('./routes/users');
const uploadRoutes = require('./routes/upload');

app.use(authRoutes.router);
app.use(daysRoutes);
app.use(storiesRoutes);
app.use(uploadRoutes);
app.use(storyPagesRoutes);
app.use(favouriteStoriesRoutes);
app.use(accomplishedActivities);
app.use(activitiesRoutes);
app.use(gainedRewards);
app.use(usersRoutes);

app.get('/home', authRoutes.requireAuth, (req, res) => {
  const str = `SELECT * FROM users WHERE user_id=?`;

  connection.query(str, [req.session.userId], (error, results) => {
    if (error) throw error;
    const [user] = results;
    if (!user) {
      res.send({ data: 'not found' });
    } else {
      res.send({ data: user });
    }
  });
});
app.get('/', (req, res) => res.send('hello'));

// app.listen(8080);
const port = process.env.PORT || 8080;
server.listen(port, (error) => {
  console.log(`Server is running on port`, port);
});

module.exports = server;
