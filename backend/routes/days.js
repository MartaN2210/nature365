const router = require('express').Router();
const bodyParser = require('body-parser');
const authRoutes = require('./auth');

const connection = require('../db_connection');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));
// router.use(authRoutes.requireAuth);

// get all days

router.get('/days', (req, res) => {
  const str = `SELECT * FROM CalendarDays`;
  connection.query(str, (err, results) => {
    if (err) throw err;
    return res.send(results);
  });
});

// get day by month

router.get('/days/:month', (req, res) => {
  const str = 'SELECT * FROM CalendarDays WHERE month = ?';
  connection.query(str, [req.params.idActivity], (err, results) => {
    if (err) throw err;
    return res.send(results);
  });
});

// get day by day of the month

router.get('/days/:month/:day', (req, res) => {
  const str =
    'SELECT c.idCalendarDay, c.introText, c.idStory, c.idActivity, a.title AS activityTitle, s.title AS storyTitle FROM CalendarDays AS c LEFT JOIN Activities AS a ON c.idActivity=a.idActivity LEFT JOIN Stories AS s ON c.idStory=s.idStory WHERE month = ? AND dayOfMonth = ?';
  connection.query(str, [req.params.month, req.params.day], (err, results) => {
    if (err) throw err;
    return res.send(results);
  });
});

// add an day content

router.post('/days', (req, res) => {
  const { introText } = req.body;
  const { day } = req.body;
  const { month } = req.body;
  const { idStory } = req.body;
  const { idActivity } = req.body;

  const sqlInsert = `INSERT INTO CalendarDays (introText, dayOfMonth, month, idStory, idActivity) VALUES (?, ?, ?, ?, ?);`;
  connection.query(
    sqlInsert,
    [introText, day, month, idStory, idActivity],
    (error, results) => {
      if (error) throw error;
      const days = results;
      console.log(days);
      return res.send({ message: 'Content successfully created' });
    }
  );
});

// update an day content

router.patch('/days/:id', (req, res) => {
  const { introText } = req.body;
  const { idStory } = req.body;
  const { idActivity } = req.body;
  const { id } = req.params;

  const sqlUpdate =
    'UPDATE CalendarDays SET introText=?, idStory=?, idActivity=? WHERE idCalendarDay = ?;';
  connection.query(
    sqlUpdate,
    [introText, idStory, idActivity, id],
    (error, results) => {
      if (error) throw error;
      const activities = results;
      console.log(activities);
      return res.send({ message: 'Content successfully updated' });
    }
  );
});

module.exports = router;
