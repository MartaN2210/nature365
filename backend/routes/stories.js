const router = require('express').Router();
const bodyParser = require('body-parser');
const authRoutes = require('./auth');

const connection = require('../db_connection');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));
// router.use(authRoutes.requireAuth);

// get all stories

router.get('/stories/', (req, res) => {
  const str = `SELECT * FROM stories`;
  connection.query(str, (err, results) => {
    if (err) throw err;
    return res.send(results);
  });
});

// get story by id

router.get('/stories/:idStory', (req, res) => {
  const str = 'SELECT * FROM stories WHERE idStory = ?';
  connection.query(str, [req.params.idStory], (err, results) => {
    if (err) throw err;
    return res.send(results);
  });
});

// add a story

router.post('/stories', (req, res) => {
  const { title } = req.body;
  const { excerpt } = req.body;
  const { fileName } = req.body;

  const sqlInsert = `INSERT INTO stories (addDate, title, excerpt, coverImageName) VALUES (current_date(),? ,? ,?);`;
  connection.query(sqlInsert, [title, excerpt, fileName], (error, results) => {
    if (error) throw error;
    const stories = results;
    console.log(stories);
    return res.send({ message: 'Story successfully created' });
  });
});

// update a story

router.patch('/stories/:idStory', (req, res) => {
  const { title } = req.body;
  const { excerpt } = req.body;
  const { fileName } = req.body;
  const { idStory } = req.params;

  const sqlUpdate =
    'UPDATE stories SET title=?, excerpt=?, coverImageName=?  WHERE idStory=?;';
  connection.query(
    sqlUpdate,
    [title, excerpt, fileName, idStory],
    (error, results) => {
      if (error) throw error;
      const stories = results;
      console.log(stories);
      return res.send({ message: 'Story successfully updated' });
    }
  );
});

// delete a story

router.delete('/stories/:idStory', (req, res) => {
  const sqlDelete = 'DELETE FROM stories WHERE idStory = ?';

  connection.query(sqlDelete, [req.params.idStory], (error, results) => {
    if (error) throw error;
    console.log(results);
    return res.send({ data: 'Got a DELETE request at /stories' });
  });
});

module.exports = router;
