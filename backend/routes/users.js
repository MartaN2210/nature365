const router = require('express').Router();
const bodyParser = require('body-parser');
require('dotenv').config(); // to use .env variables
const bcrypt = require('bcrypt');
const authRoutes = require('./auth');

const saltRounds = 10;

const connection = require('../db_connection');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));
// router.use(authRoutes.requireAuth);

// get all users

router.get('/users', (req, res) => {
  const str = `SELECT user_id, user_email, first_name, last_name FROM users`;
  connection.query(str, (err, results) => {
    if (err) throw err;
    return res.send(results);
  });
});

// update a single user

router.patch('/users/:user_id', (req, res) => {
  const { password } = req.body;

  const sqlUpdate =
    'UPDATE users SET first_name = ?, last_name= ?, password= ? WHERE user_id = ?';

  bcrypt.hash(password, saltRounds).then((hashPassword) => {
    connection.query(
      sqlUpdate,
      [
        req.body.first_name,
        req.body.last_name,
        hashPassword,
        req.params.user_id,
      ],
      (error, results) => {
        if (error) throw error;
        res.send({
          message: 'Your profila has been updated successfully',
        });
      }
    );
  });
});

module.exports = router;
