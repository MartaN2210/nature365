const router = require('express').Router();
const bodyParser = require('body-parser');
const authRoutes = require('./auth');

const connection = require('../db_connection');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));
// router.use(authRoutes.requireAuth);

// get rewards filtered by user

router.get('/gainedrewards/filter', (req, res) => {
  const str =
    'SELECT * FROM gainedRewards LEFT JOIN rewards ON gainedRewards.idReward = rewards.idReward WHERE idUser = ?';
  connection.query(str, [req.query.idUser], (err, results) => {
    if (err) throw err;
    console.log(req.query.idUser);

    return res.send(results);
  });
});

// add a reward

router.post('/gainedrewards', (req, res) => {
  const { idUser } = req.body;
  const { idReward } = req.body;

  const sqlInsert = `INSERT INTO GainedRewards (addDate, idUser, idReward) VALUES (current_date(),?, ?);`;
  connection.query(sqlInsert, [idUser, idReward], (error, results) => {
    if (error) throw error;
    const gainedRewards = results;
    console.log(gainedRewards);
    return res.send({ message: 'Reward successfully added' });
  });
});

module.exports = router;
