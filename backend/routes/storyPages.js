const router = require('express').Router();
const bodyParser = require('body-parser');
const authRoutes = require('./auth');

const connection = require('../db_connection');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));
// router.use(authRoutes.requireAuth);

// get all story pages

router.get('/storyPages/', (req, res) => {
  const str = `SELECT * FROM StoryPages`;
  connection.query(str, (err, results) => {
    if (err) throw err;
    return res.send(results);
  });
});

// get story page by id

// router.get('/storyPages/:idStoryPage', (req, res) => {
//   const str = 'SELECT * FROM StoryPages WHERE idStoryPage = ?';
//   connection.query(str, [req.params.idStoryPage], (err, results) => {
//     if (err) throw err;
//     // console.log(req.query)
//     return res.send(results);
//   });
// });

// get story pages filtered

router.get('/storyPages/filter?', (req, res) => {
  const str = 'SELECT * FROM StoryPages WHERE idStory = ?';
  connection.query(str, [req.query.idStory], (err, results) => {
    if (err) throw err;
    console.log(results);
    return res.send(results);
  });
});

// add a story page

router.post('/storyPages', (req, res) => {
  const { text } = req.body;
  const { fileName } = req.body;
  const { idStory } = req.body;

  const sqlInsert = `INSERT INTO StoryPages (text, imageFileName, idStory) VALUES (? ,? ,?);`;
  connection.query(sqlInsert, [text, fileName, idStory], (error, results) => {
    if (error) throw error;
    const stories = results;
    console.log(stories);
    return res.send({ message: 'Story page successfully created' });
  });
});

// update a story page

router.patch('/StoryPages/:idStoryPage', (req, res) => {
  const { text } = req.body;
  const { fileName } = req.body;

  const sqlUpdate =
    'UPDATE StoryPages SET text=?, imageFileName=?  WHERE idStoryPage=?;';
  connection.query(
    sqlUpdate,
    [text, fileName, req.params.idStoryPage],
    (error, results) => {
      if (error) throw error;
      const stories = results;
      console.log(stories);
      return res.send({ message: 'Story page successfully updated' });
    }
  );
});

// delete a story page

router.delete('/StoryPages/:idStoryPage', (req, res) => {
  const sqlDelete = 'DELETE FROM storyPages WHERE idStoryPage = ?';

  connection.query(sqlDelete, [req.params.idStoryPage], (error, results) => {
    if (error) throw error;
    console.log(results);
    return res.send({ data: 'Got a DELETE request at /storyPages' });
  });
});

module.exports = router;
