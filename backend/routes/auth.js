const router = require('express').Router();
const bodyParser = require('body-parser');
require('dotenv').config(); // to use .env variables
const bcrypt = require('bcrypt');
const nodemailer = require('nodemailer');
const jwt = require('jsonwebtoken');
const passport = require('passport');
const connection = require('../db_connection');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));

const saltRounds = 10;

require('../passportConfig')(passport);

const requireAuth = (req, res, next) => {
  console.log('mind', req.session, req.isAuthenticated());
  if (req.isAuthenticated()) {
    next();
  }
};

router.use(passport.initialize());
router.use(passport.session());
const initializePassport = require('../passportConfig');

initializePassport(
  passport,

  (email) =>
    new Promise((resolve, reject) => {
      try {
        const str = `SELECT * FROM users WHERE email=?`;
        connection.query(str, [email], (error, results) => {
          console.log('function is called');
          if (error) throw error;
          const [user] = results;
          if (user) {
            resolve(user);
          } else {
            resolve();
          }
        });
      } catch (err) {
        reject(err);
      }
    })
);

router.post('/auth/login', (req, res, next) => {
  passport.authenticate('local', (err, user, info) => {
    if (err) throw err;
    if (!user) res.status(403).json({ message: 'Wrong user name or password' });
    else {
      req.logIn(user, (err) => {
        if (err) throw err;
        res.send(req.user);
        console.log(req.session.id);
      });
    }
  })(req, res, next);
});

function validateNewUser(req, res, next) {
  const { email } = req.body;
  const strSelect = `SELECT * FROM users WHERE email=?`;

  connection.query(strSelect, [email], (error, results) => {
    if (error) throw error;
    const [user] = results;
    if (user) {
      res.status(400).send({ message: `User already exists` });
    } else {
      next();
    }
  });
}

router.post('/auth/signup', validateNewUser, (req, res) => {
  const { name, email, password } = req.body;
  const strToInsert = `INSERT INTO users (name, email, password) VALUES (?, ? ,?)`;

  bcrypt.hash(password, saltRounds).then((hashPassword) => {
    connection.query(strToInsert, [name, email, hashPassword], (error) => {
      if (error) throw error;
      res.status(200).send({ message: `User created successfully` });
    });
  });
});

router.post('/auth/new_password', (req, res) => {
  const { password } = req.body;
  const email = req.session.userEmail;
  const sqlUpdateUser = `UPDATE users SET password = ? WHERE email = ?`;

  bcrypt.hash(password, saltRounds).then((hashPassword) => {
    connection.query(sqlUpdateUser, [hashPassword, email], (error) => {
      if (error) throw error;
      res.status(200).send({ message: `User updated successfully` });
    });
  });
});

router.post('/auth/add_user', requireAuth, validateNewUser, (req, res) => {
  const timestamp = Date.now();

  const user = {
    email: req.body.email,
    timestamp: timestamp,
    token_number: '',
    confirmed: 0,
  };

  const token = jwt.sign({ user }, process.env.JWT_ACC_ACTIVATE, {
    expiresIn: '20m',
  });

  user.token_number = token;

  const str = `INSERT INTO email_verifications (email, token_number, timestamp, confirmed) VALUES (?, ?, ?, ?); INSERT INTO users (email) VALUES (?)`;

  connection.query(
    str,
    [user.email, user.token_number, timestamp, user.confirmed, user.email],
    (err, result) => {
      if (err) throw err;
    }
  );

  nodemailer.createTestAccount((err, _account) => {
    const htmlEmail = `<h3> Please click and follow the link to create your account:
      <a href='${process.env.BACKEND_URL}/auth/email_verification/${token}'>${process.env.BACKEND_URL}/auth/email_verification/${token}</a>
      </h3>`;
    const transporter = nodemailer.createTransport({
      host: 'smtp.ethereal.email',
      port: 587,
      auth: {
        user: 'matteo.gusikowski36@ethereal.email',
        pass: 'TwXFEYmA4pwQdzafha',
      },
    });
    const mailOptions = {
      from: 'test@test-account.com',
      to: 'glenda.nikolaus46@ethereal.email',
      replyTo: 'test@test-account.com',
      subject: 'Confirm Email',
      text: 'sth',
      html: htmlEmail,
    };

    transporter.sendMail(mailOptions, (err, _info) => {
      if (err) {
        console.log(err);
      }
    });
  });
});

router.get('/auth/email_verification/:token', (req, res) => {
  const { token } = req.params;
  if (token) {
    jwt.verify(token, process.env.JWT_ACC_ACTIVATE, (err, decodedToken) => {
      if (err) {
        return res.status(400).json({ error: 'Incorrect or expired link. ' });
      }
      const passedUserData = decodedToken;
      passedUserData.user.tokenNumber = token;
      passedUserData.user.confirmed = 1;
      const { tokenNumber } = passedUserData.user;
      const { confirmed } = passedUserData.user;
      if (
        passedUserData.user.email &&
        tokenNumber &&
        passedUserData.user.timestamp &&
        confirmed
      ) {
        const updateConfirmed = 1;
        const str = `UPDATE email_verifications SET confirmed = ? WHERE email = ? AND token_number= ? AND timestamp= ?`;
        connection.query(
          str,
          [
            updateConfirmed,
            passedUserData.user.email,
            tokenNumber,
            passedUserData.user.timestamp,
          ],
          (error, results, fields) => {
            if (!results) {
              return res.status(501).send({ message: `Something went wrong` });
            }
            req.session.userEmail = passedUserData.user.email;
            return res.redirect('http://localhost:3000/new_password');
          }
        );
      }
    });
  } else {
    return res.json({ error: 'Something went wrong' });
  }
});

router.get('/auth/is_auth', (req, res) => {
  if (!req.isAuthenticated()) {
    console.log('is not auth');
    res.send({ auth: false });
  } else {
    console.log('is  auth');
    res.send(req.user);
  }
});

router.get('/auth/logout', (req, res) => {
  req.session.destroy();
  res.send({ auth: false });
});

module.exports = {
  router,
  requireAuth,
};
