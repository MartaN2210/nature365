const router = require('express').Router();
const bodyParser = require('body-parser');
const authRoutes = require('./auth');

const connection = require('../db_connection');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));
// router.use(authRoutes.requireAuth);

// get all activities

router.get('/activities', (req, res) => {
  const str = `SELECT * FROM activities`;
  connection.query(str, (err, results) => {
    if (err) throw err;
    return res.send(results);
  });
});

// get activity by id

router.get('/activities/:idActivity', (req, res) => {
  const str = 'SELECT * FROM activities WHERE idActivity = ?';
  connection.query(str, [req.params.idActivity], (err, results) => {
    if (err) throw err;
    return res.send(results);
  });
});

// add an activity

router.post('/activities', (req, res) => {
  const { title } = req.body;
  const { description } = req.body;
  const { fileName } = req.body;

  const sqlInsert = `INSERT INTO activities (addDate, title, description, coverImageName) VALUES (current_date(),? ,? ,?);`;
  connection.query(
    sqlInsert,
    [title, description, fileName],
    (error, results) => {
      if (error) throw error;
      const activities = results;
      console.log(activities);
      return res.send({ message: 'Activity successfully created' });
    }
  );
});

// update an activity

router.patch('/activities/:idActivity', (req, res) => {
  const { title } = req.body;
  const { description } = req.body;
  const { fileName } = req.body;
  const { idActivity } = req.params;

  const sqlUpdate =
    'UPDATE activities SET title=?, description=?, coverImageName=? WHERE idActivity=?;';
  connection.query(
    sqlUpdate,
    [title, description, fileName, idActivity],
    (error, results) => {
      if (error) throw error;
      const activities = results;
      console.log(activities);
      return res.send({ message: 'Activity successfully updated' });
    }
  );
});

// delete a story

router.delete('/activities/:idActivity', (req, res) => {
  const sqlDelete = 'DELETE FROM activities WHERE idActivity = ?';

  connection.query(sqlDelete, [req.params.idActivity], (error, results) => {
    if (error) throw error;
    console.log(results);
    return res.send({ data: 'Got a DELETE request at /activities' });
  });
});

module.exports = router;
