const router = require('express').Router();
const bodyParser = require('body-parser');
const authRoutes = require('./auth');

const connection = require('../db_connection');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));
// router.use(authRoutes.requireAuth);

// get all favouriteStories

router.get('/favouritestories', (req, res) => {
  const str = `SELECT * FROM favouriteStories`;
  connection.query(str, (err, results) => {
    if (err) throw err;
    return res.send(results);
  });
});

// get story by id

router.get('/favouritestories/:idStory/:idUser', (req, res) => {
  const str = 'SELECT * FROM favouriteStories WHERE idStory = ? AND idUser = ?';
  connection.query(
    str,
    [req.params.idStory, req.params.idUser],
    (err, results) => {
      if (err) throw err;
      return res.send(results);
    }
  );
});

router.get('/favouritestories/filter', (req, res) => {
  const str =
    'SELECT * FROM favouriteStories LEFT JOIN stories ON favouriteStories.idStory = stories.idStory WHERE idUser = ?';
  connection.query(str, [req.query.idUser], (err, results) => {
    if (err) throw err;
    console.log(req.query.idUser);

    return res.send(results);
  });
});

// add a story

router.post('/favouritestories', (req, res) => {
  const { idUser } = req.body;
  const { idStory } = req.body;

  const sqlInsert = `INSERT INTO favouriteStories (addDate, idUser, idStory) VALUES (current_date(),?, ?);`;
  connection.query(sqlInsert, [idUser, idStory], (error, results) => {
    if (error) throw error;
    const favouriteStories = results;
    console.log(favouriteStories);
    return res.send({ message: 'Story successfully created' });
  });
});

// delete a story

router.delete('/favouritestories/:idStory/:idUser', (req, res) => {
  const sqlDelete =
    'DELETE FROM favouriteStories WHERE idStory = ? AND idUser = ?';

  connection.query(
    sqlDelete,
    [req.params.idStory, req.params.idUser],
    (error, results) => {
      if (error) throw error;
      console.log(results);
      return res.send({ data: 'Got a DELETE request at /favouritestories' });
    }
  );
});

module.exports = router;
