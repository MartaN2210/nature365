const router = require('express').Router();
const bodyParser = require('body-parser');
const authRoutes = require('./auth');

const connection = require('../db_connection');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));
// router.use(authRoutes.requireAuth);

// get all accomplished activities

router.get('/accomplishedactivities', (req, res) => {
  const str = `SELECT * FROM accomplishedActivities`;
  connection.query(str, (err, results) => {
    if (err) throw err;
    return res.send(results);
  });
});

// get story by id

router.get('/accomplishedactivities/:idActivity/:idUser', (req, res) => {
  const str =
    'SELECT * FROM accomplishedActivities WHERE idActivity = ? AND idUser = ?';
  connection.query(
    str,
    [req.params.idActivity, req.params.idUser],
    (err, results) => {
      if (err) throw err;
      return res.send(results);
    }
  );
});

// get all accomplished activities by user id within current month

router.get('/accomplishedactivities/filter', (req, res) => {
  const date = new Date();
  const month = date.getMonth() + 1;
  const str = `SELECT * FROM accomplishedActivities WHERE idUser = ? AND month(addDate)=${month}`;
  connection.query(str, [req.query.idUser], (err, results) => {
    if (err) throw err;
    console.log(month);

    return res.send(results);
  });
});

// add an accomplished activity

router.post('/accomplishedactivities', (req, res) => {
  const { idUser } = req.body;
  const { idActivity } = req.body;

  const sqlInsert = `INSERT INTO accomplishedActivities (addDate, idUser, idActivity) VALUES (current_date(),?, ?);`;
  connection.query(sqlInsert, [idUser, idActivity], (error, results) => {
    if (error) throw error;
    const accomplishedActivities = results;
    console.log(accomplishedActivities);
    return res.send({ message: 'Activity successfully created' });
  });
});

module.exports = router;
