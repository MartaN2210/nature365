import React from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import ProgressLine from '../../../components/ProgressLine/ProgressLine';
import './progressPage.scss';
import img from '../../../assets/images/animal_success.jpg';

const ProgressPage = () => (
  <Container className="progress-content shadow">
    <Row className="align-items-center">
      <Col xs={12} className="col">
        <img src={img} alt="success illustration" />
        <h1 className="w-100 text-center">Congratulations !!!!</h1>
        <ProgressLine className="mt-5" />
      </Col>
    </Row>
  </Container>
);

export default ProgressPage;
