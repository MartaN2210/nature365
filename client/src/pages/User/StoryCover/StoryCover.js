import React, { useState, useContext, useEffect } from 'react';
import { Container, Row, Col, Button } from 'react-bootstrap';
import { BsFillStarFill } from 'react-icons/bs';
import { Link, useParams } from 'react-router-dom';
import axios from 'axios';
import { DayContentApi } from '../../../context/DayContentApi';
import FavStar from '../../../components/FavStar/FavStar';
import './storyCover.scss';

const StoryCover = () => {
  const { REACT_APP_API_URL } = process.env;
  const [dayContent] = useContext(DayContentApi);
  const [story, setStory] = useState('');
  const { id } = useParams();
  console.log(id);
  useEffect(() => {
    loadData();
  }, [dayContent]);

  const loadData = async () => {
    axios
      .get(
        `${REACT_APP_API_URL}/stories/${
          id || (dayContent ? dayContent.idStory : '17')
        }`
      )
      .then((res) => {
        console.log(res.data);
        setStory(res.data[0]);
      });
  };

  return (
    <Container className="story-cover-content p-0">
      <Row className="m-0">
        <Col
          xs={12}
          className="col p-0"
          style={{
            backgroundImage: `url('${process.env.PUBLIC_URL}/uploads/${
              story ? story.coverImageName : ''
            }')`,
            backgroundSize: 'cover',
            backgroundPosition: 'center',
          }}
        >
          <div className="dark-overlay">
            <FavStar idStory={id || (dayContent ? dayContent.idStory : '17')} />
            <div className="d-flex flex-column align-items-center w-100">
              <h2 className="w-100 text-center">{story.title}</h2>
              <Link to={`/story/${story.idStory}`}>
                <Button className="text-center">Start</Button>
              </Link>
            </div>
          </div>
        </Col>
      </Row>
    </Container>
  );
};

export default StoryCover;
