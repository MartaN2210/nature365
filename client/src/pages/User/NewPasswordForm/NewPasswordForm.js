import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';

export default function NewPasswordForm(props) {
  const [message, setMessage] = useState();
  const [password, setPassword] = useState();

  const history = useHistory();

  const handleInputPasswordChanged = (event) => {
    setPassword(event.target.value);
  };

  const handleFormSubmit = (event) => {
    event.preventDefault();
    const requestOptions = {
      method: 'POST',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
      body: JSON.stringify({
        password: password,
      }),
    };

    // create new password:
    (async () => {
      try {
        const response = await fetch(
          'http://localhost:3000/auth/new_password',
          requestOptions
        );
        if (!response.ok) {
          if (response.status === 400 || response.status === 429) {
            const data = await response.json();
            setMessage(data.message);
          } else {
            throw new Error(`HTTP error! status: ${response.status}`);
          }
        } else {
          history.push('/login');
        }
      } catch (error) {
        console.error(error);
        setMessage('Ups! Something went wrong. Please try again');
      }
    })();
  };

  return (
    <div>
      <form onSubmit={handleFormSubmit}>
        <h2>New password</h2>
        <input
          id="password"
          placeholder="password"
          onChange={handleInputPasswordChanged}
        />
        <button type="submit">Submit</button>
      </form>
      {message ? <p>{message}</p> : ''}
      <a href="/login">log in</a>
    </div>
  );
}
