import React, { useContext, useEffect, useState } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { BsCardImage } from 'react-icons/bs';
import { Link } from 'react-router-dom';
import { FaBookReader, FaChild } from 'react-icons/fa';
import axios from 'axios';
import { AuthApi } from '../../../context/AuthApi';
import { DayContentApi } from '../../../context/DayContentApi';
import './home.scss';

const Home = () => {
  const { REACT_APP_API_URL } = process.env;
  const currentDay = new Date();
  const [auth] = useContext(AuthApi);
  const [dayContent] = useContext(DayContentApi);
  const [story, setStory] = useState();

  console.log(dayContent);
  useEffect(() => {
    loadData();
  }, [dayContent]);

  const loadData = async () => {
    const config = {
      headers: { crossDomain: true, 'Content-Type': 'application/json' },
      withCredentials: true,
      crossdomain: true,
    };
    axios
      .get(
        `${REACT_APP_API_URL}/stories/${
          dayContent ? dayContent.idStory : '17'
        }`,
        config
      )
      .then((res) => {
        setStory(res.data[0]);
      })
      .catch((err) => console.error(err));
  };
  return (
    <Container className="home-content shadow p-0">
      <Row>
        <Col
          xs={12}
          md={6}
          className="d-flex justify-content-center align-items-center"
        >
          {story ? (
            <img
              className="cover-img"
              src={`${process.env.PUBLIC_URL}/uploads/${story.coverImageName}`}
              alt="cover img"
            />
          ) : (
            <BsCardImage size={65} />
          )}
        </Col>
        <Col
          xs={12}
          md={6}
          className="d-flex flex-column justify-content-center align-items-center p-md-5 p-5"
        >
          <h1 className="w-100 mb-3 mb-md-5">{`Hi,${
            auth ? auth.name : ''
          }`}</h1>
          <p className="pb-md-5">{dayContent ? dayContent.introText : ''}</p>
          <div className="d-none d-md-flex w-100">
            <Link
              className="btn btn-primary mr-2 d-flex align-items-center"
              to="/storyCover"
            >
              <FaBookReader className="mr-3" color="#fffffc" size={25} />
              Todays story
            </Link>
            <Link
              className="btn btn-primary d-flex align-items-center"
              to="/activity"
            >
              <FaChild className="mr-3" color="#fffffc" size={25} />
              Todays activity
            </Link>
          </div>
        </Col>
      </Row>
      <div className="date-container">
        <div className="day">{currentDay.getDate()}</div>
        <div className="month">
          {currentDay.toLocaleString('default', {
            month: 'long',
          })}
        </div>
      </div>
    </Container>
  );
};

export default Home;
