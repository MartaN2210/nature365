import React, { useContext, useState } from 'react';
import { Form, Button, Container, Row, Col } from 'react-bootstrap';
import { AuthApi } from '../../../App/AuthApi';
import './editProfile.css';

export default function EditProfile() {
  const [auth] = useContext(AuthApi);
  const [firstName, setFirstName] = useState(auth.first_name);
  const [lastName, setLastName] = useState(auth.last_name);
  const [password, setPassword] = useState();
  const [email] = useState(auth.user_email);
  const [message, setMessage] = useState();

  const handleInputFirstNameChanged = (event) => {
    setFirstName(event.target.value);
  };

  const handleInputLastNameChanged = (event) => {
    setLastName(event.target.value);
  };

  const handleInputPasswordChanged = (event) => {
    setPassword(event.target.value);
  };

  const handleFormSubmit = (event, userId) => {
    event.preventDefault();
    return fetch(`/users/${auth.user_id}`, {
      method: 'PATCH',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        first_name: firstName,
        last_name: lastName,
        password: password,
      }),
    })
      .then((res) => res.json())
      .then((res) => {
        setMessage(res.message);
      })
      .catch((err) => console.error(err));
  };

  return (
    <Container className="main-container edit-profile-container">
      <h2>Edit profile</h2>
      <Row>
        <Col>
          <Form onSubmit={(event) => handleFormSubmit(event, auth.user_id)}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>First name</Form.Label>
              <Form.Control
                type="text"
                placeholder="e.g. John"
                value={firstName || ''}
                onChange={handleInputFirstNameChanged}
              />
            </Form.Group>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Last name</Form.Label>
              <Form.Control
                type="text"
                placeholder="e.g. Smith"
                value={lastName || ''}
                onChange={handleInputLastNameChanged}
              />
            </Form.Group>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="email"
                placeholder="e.g. john@gmail.com"
                value={email || ''}
                readOnly
              />
            </Form.Group>
            <Form.Group controlId="formBasicPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                onChange={handleInputPasswordChanged}
                required
              />
            </Form.Group>
            {message ? <p>{message}</p> : ''}
            <Button variant="primary" type="submit">
              Save
            </Button>
          </Form>
        </Col>
      </Row>
    </Container>
  );
}
