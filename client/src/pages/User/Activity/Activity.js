import React, { useContext, useState, useEffect } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { BsCardImage } from 'react-icons/bs';
import axios from 'axios';
import DragButton from '../../../components/DragButton/DragButton';
import { DayContentApi } from '../../../context/DayContentApi';
import './activity.scss';

const Activity = () => {
  const { REACT_APP_API_URL } = process.env;
  const [dayContent] = useContext(DayContentApi);
  const [activity, setActivity] = useState();

  useEffect(() => {
    loadData();
  }, [dayContent]);

  const loadData = async () => {
    axios
      .get(
        `${REACT_APP_API_URL}/activities/${
          dayContent ? dayContent.idActivity : ''
        }`
      )
      .then((res) => {
        setActivity(res.data[0]);
        console.log(res.data);
      })
      .catch((err) => console.error(err));
  };
  return (
    <Container className="activity-content shadow">
      <Row>
        <Col
          xs={12}
          md={6}
          className="col d-flex justify-content-center align-items-center"
        >
          <div className="img-wrap d-flex justify-content-center align-items-center">
            {activity ? (
              <img
                src={`${process.env.PUBLIC_URL}/uploads/${activity.coverImageName}`}
                alt="cover img"
              />
            ) : (
              <BsCardImage size={65} />
            )}
          </div>
        </Col>
        <Col
          xs={12}
          md={6}
          className="col d-flex flex-column justify-content-center p-4 p-md-5"
        >
          <h2 className="w-100">{activity ? activity.title : ''}</h2>
          <p>{activity ? activity.description : ''}</p>
          <div className="d-flex w-100 d-flex justify-content-start mt-md-5">
            <DragButton />
          </div>
        </Col>
      </Row>
    </Container>
  );
};

export default Activity;
