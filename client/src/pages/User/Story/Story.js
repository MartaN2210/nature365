import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Carousel } from 'react-bootstrap';
import axios from 'axios';
import { useParams } from 'react-router-dom';
import FavStar from '../../../components/FavStar/FavStar';
import './story.scss';

const Story = () => {
  const { REACT_APP_API_URL } = process.env;
  const [storyPages, setStoryPages] = useState();
  const { id } = useParams();

  useEffect(() => {
    axios
      .get(`${REACT_APP_API_URL}/storyPages/filter?idStory=${id}`)
      .then((res) => {
        setStoryPages(res.data);
      })
      .catch((err) => console.error(err));
  }, []);

  return (
    <Container className="story-content p-0">
      <Row className="m-0">
        <Col xs={12} className="col p-0">
          <FavStar idStory={id} />
          <Carousel interval={null} indicators={false}>
            {storyPages
              ? storyPages.map((page) => (
                  <Carousel.Item>
                    <div className="image w-100">
                      <img
                        src={`${process.env.PUBLIC_URL}/uploads/${page.imageFileName}`}
                        alt="illustration img"
                      />
                    </div>
                    <p className="text">{page.text}</p>
                  </Carousel.Item>
                ))
              : ''}
          </Carousel>
        </Col>
      </Row>
    </Container>
  );
};

export default Story;
