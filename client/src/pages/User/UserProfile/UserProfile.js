import React, { useContext } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import GainedRewards from '../../../components/GainedRewards/GainedRewards';
import ProgressLine from '../../../components/ProgressLine/ProgressLine';
import { AuthApi } from '../../../context/AuthApi';

import './userProfile.scss';

const UserProfile = () => {
  const date = new Date();
  const [auth] = useContext(AuthApi);

  return (
    <Container className="user-profile-content shadow p-5">
      <h2 className="mb-5">Your Profile</h2>
      <Row>
        <Col xs={12} md={6} className="col d-flex flex-column">
          <div>
            <p className="small">User name</p>
            <p>{auth.name}</p>
          </div>
          <div>
            <p className="small">email</p>
            <p>{auth.email}</p>
          </div>
        </Col>
        <Col xs={12} md={6} className="col d-flex flex-column">
          <div className="mb-5">
            <p className="w-100 font-weight-bold pb-3">
              Your current challenge
            </p>
            <p>
              {`${date.toLocaleString('default', {
                month: 'long',
              })} challenge`}
            </p>
            <ProgressLine isProfilePage />
          </div>
          <div>
            <p className="w-100 font-weight-bold pb-3">Your awards</p>
            <GainedRewards />
          </div>
        </Col>
      </Row>
      <div className="d-flex justify-content-center">
        <Link className="btn btn-primary logout-btn" to="/logout">
          Logout
        </Link>
      </div>
    </Container>
  );
};

export default UserProfile;
