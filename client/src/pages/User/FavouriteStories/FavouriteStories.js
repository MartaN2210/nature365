import React, { useState, useEffect, useContext } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import axios from 'axios';
import { AuthApi } from '../../../context/AuthApi';
import './favouriteStories.scss';

const FavouriteStories = () => {
  const [favouriteStories, setFavouriteStories] = useState();
  const [auth] = useContext(AuthApi);

  const { REACT_APP_API_URL } = process.env;
  useEffect(() => {
    loadData();
  }, []);

  const loadData = async () => {
    axios
      .get(`${REACT_APP_API_URL}/favouritestories/filter?idUser=${auth.idUser}`)
      .then((res) => {
        console.log('fav', res.data);
        setFavouriteStories(res.data);
      })
      .catch((err) => console.error(err));
  };

  return (
    <Container className="favourite-stories-content">
      <h2 className="text-center">Favourite stories</h2>
      <Row>
        {favouriteStories
          ? favouriteStories.map((story) => (
              <Col
                xs={6}
                md={4}
                lg={3}
                className="col d-flex justify-content-center align-items-center mb-3"
              >
                <a
                  href={`/chosenStoryCover/${story.idStory}`}
                  className="card-container w-100 d-flex align-items-end shadow"
                >
                  <img
                    src={`${process.env.PUBLIC_URL}/uploads/${story.coverImageName}`}
                    alt={`${story.title}-cover`}
                  />
                  <div className="title-container d-flex align-items-center justify-content-center">
                    <p className="text-center">{story.title}</p>
                  </div>
                </a>
              </Col>
            ))
          : ''}
      </Row>
    </Container>
  );
};

export default FavouriteStories;
