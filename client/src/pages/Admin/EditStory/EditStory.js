import React, { useState, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';
import axios from 'axios';
import { useParams } from 'react-router-dom';
import './editStory.scss';

const EditStory = () => {
  const { REACT_APP_API_URL } = process.env;
  const { id } = useParams();
  const [selectedFile, setSelectedFile] = useState(null);
  const [selectedPageFile, setSelectedPageFile] = useState(null);
  const [pageText, setPageText] = useState('');
  const [title, setTitle] = useState('');
  const [excerpt, setExcerpt] = useState('');
  const [message, setMessage] = useState();
  const [coverImageName, setCoverImageName] = useState(null);
  const [pages, setPages] = useState();

  useEffect(() => {
    loadData();
  }, []);

  const uploadFileHandler = (event) => {
    setSelectedFile(event.target.files[0]);
  };

  const changeTitleHandler = (event) => {
    setTitle(event.target.value);
  };

  const changeExcerptHandler = (event) => {
    setExcerpt(event.target.value);
  };

  const onClickHandler = (event) => {
    event.preventDefault();
    const currentDate = Date.now();
    const data = new FormData();
    let newName;
    if (selectedFile) {
      newName = `${currentDate}-${selectedFile.name}`;
      data.append('file', selectedFile, `${newName}`);
      axios.post(`${REACT_APP_API_URL}/upload`, data).then((res) => {
        console.log(res.statusText);
      });
    }
    axios
      .patch(`${REACT_APP_API_URL}/stories/${id}`, {
        title: title,
        excerpt: excerpt,
        fileName: newName || coverImageName,
      })
      .then((res) => {
        setMessage(res.data.message);
        loadData();
      })
      .catch((err) => console.error(err));
  };

  const changePageTextHandler = (event) => {
    setPageText(event.target.value);
  };

  const uploadPageFileHandler = (event) => {
    setSelectedPageFile(event.target.files[0]);
  };

  const addPageHandler = (event) => {
    event.preventDefault();
    console.log('add page');
    const currentDate = Date.now();
    const data = new FormData();
    let newName;
    if (selectedPageFile) {
      newName = `${currentDate}-${selectedPageFile.name}`;
      data.append('file', selectedPageFile, `${newName}`);
      axios.post(`${REACT_APP_API_URL}/upload`, data).then((res) => {
        console.log(res.statusText);
      });
    }

    axios
      .post(`${REACT_APP_API_URL}/storyPages`, {
        text: pageText,
        fileName: newName || '',
        idStory: id,
      })
      .then((res) => {
        setMessage(res.data.message);
        setPageText('');
        setSelectedPageFile('');
        loadData();
      })
      .catch((err) => console.error(err));
  };

  const loadData = () => {
    axios
      .get(`${REACT_APP_API_URL}/stories/${id}`)
      .then((res) => {
        setExcerpt(res.data[0].excerpt);
        setTitle(res.data[0].title);
        setCoverImageName(res.data[0].coverImageName);
        console.log(res.data);
      })
      .catch((err) => console.error(err));

    axios
      .get(`${REACT_APP_API_URL}/storyPages/filter?idStory=${id}`)
      .then((res) => {
        setPages(res.data);
        console.log('story pages', res.data);
      })
      .catch((err) => console.error(err));
  };

  return (
    <div className="main container edit-story-form overflow-scroll">
      <div className="row mb-4">
        <div className="col-sm-12">
          <h1>Edit story</h1>
          <Form>
            <Form.Group controlId="formStoryTitle" className="mb-3">
              <Form.Label>Story title</Form.Label>
              <Form.Control
                onChange={(event) => changeTitleHandler(event)}
                type="text"
                placeholder="Story title"
                value={title}
              />
            </Form.Group>
            <Form.Group controlId="formStoryExcerpt" className="mb-3">
              <Form.Label>Story excerpt</Form.Label>
              <Form.Control
                onChange={(event) => changeExcerptHandler(event)}
                as="textarea"
                rows={3}
                value={excerpt}
              />
            </Form.Group>
            <div className="img-thumbnail-wrap mb-3">
              <img
                src={`${process.env.PUBLIC_URL}/uploads/${coverImageName}`}
                alt="cover-img"
              />
            </div>
            <Form.Group className="mb-3">
              <Form.File
                onChange={(event) => uploadFileHandler(event)}
                id="exampleFormControlFile1"
                label="Update cover image"
              />
            </Form.Group>
            <Button
              variant="primary"
              type="submit"
              onClick={(event) => onClickHandler(event)}
            >
              Update
            </Button>
          </Form>
          {message ? <p>{message}</p> : ''}
        </div>
      </div>
      <div className="row mb-5 pb-5">
        <div className="col-sm-12">
          <h2>Story pages</h2>
          <div className="container story-pages-content mb-5">
            <div className="row story-row">
              <div className="col-sm-6">Text</div>
              <div className="col-sm-4">Image</div>
              <div className="col-sm-2" />
            </div>
            {pages
              ? pages.map((page) => (
                  <div className="row story-row">
                    <div className="col-sm-6">
                      <Form.Control
                        onChange={(event) => changePageTextHandler(event)}
                        as="textarea"
                        rows={3}
                        value={page.text}
                      />
                    </div>
                    <div className="col-sm-4">
                      <img
                        src={`${process.env.PUBLIC_URL}/uploads/${page.imageFileName}`}
                        alt="page-img"
                      />
                      <Form.File
                        onChange={(event) => uploadPageFileHandler(event)}
                        id="addFile1"
                        label="Add page illustration"
                      />
                    </div>
                  </div>
                ))
              : ''}
            <div className="row">
              <div className="col-sm-6">
                <Form.Control
                  onChange={(event) => changePageTextHandler(event)}
                  as="textarea"
                  rows={3}
                  value={pageText}
                />
              </div>
              <div className="col-sm-4">
                <Form.File
                  onChange={(event) => uploadPageFileHandler(event)}
                  id="exampleFormControlFile1"
                  label="Add page illustration"
                />
              </div>
              <div className="col-sm-2">
                <Button onClick={addPageHandler}>Add page</Button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default EditStory;
