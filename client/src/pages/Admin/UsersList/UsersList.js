import React, { useState, useEffect } from 'react';
import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';

import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';
import Loader from 'react-loader-spinner';

export default function UsersList() {
  const [isLoading, setIsLoading] = useState(true);
  const [users, setUsers] = useState([]);

  useEffect(() => {
    setIsLoading(false);
    loadData();
  }, []);

  const loadData = async () => {
    const api = 'http://localhost:8080/users';
    const response = await fetch(api);
    const data = await response.json();
    setUsers(data);
  };

  return (
    <div className="main container">
      <div className="row">
        <div className="col-sm-2" />
        <div className="col-sm-8">
          <h1>Users</h1>
          {isLoading ? (
            <Loader type="Oval" color="#00BFFF" />
          ) : (
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>Created at:</th>
                  <th>User ID</th>
                  <th>Email:</th>
                  <th>FirstName</th>
                  <th>LastName</th>
                  <th>Remove</th>
                </tr>
              </thead>
              <tbody>
                {users.map((user) => (
                  <tr key={user.user_id}>
                    <td>missing timestamp</td>
                    <td>{user.user_id}</td>
                    <td>{user.user_email}</td>
                    <td>{user.first_name}</td>
                    <td>{user.last_name}</td>
                    <td>
                      <Button variant="danger">Remove</Button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          )}
        </div>
        <div className="col-sm-2" />
      </div>
    </div>
  );
}
