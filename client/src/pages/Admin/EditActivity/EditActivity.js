import React, { useState, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';
import axios from 'axios';
import { useParams } from 'react-router-dom';

const EditActivity = () => {
  const { REACT_APP_API_URL } = process.env;
  const { id } = useParams();
  const [selectedFile, setSelectedFile] = useState(null);
  const [title, setTitle] = useState('');
  const [description, setDescription] = useState('');
  const [coverImageName, setCoverImageName] = useState(null);
  const [message, setMessage] = useState();

  useEffect(() => {
    loadData();
  }, []);

  const uploadFileHandler = (event) => {
    setSelectedFile(event.target.files[0]);
  };

  const changeTitleHandler = (event) => {
    setTitle(event.target.value);
  };

  const changeDescriptionHandler = (event) => {
    setDescription(event.target.value);
  };

  const onClickHandler = (event) => {
    event.preventDefault();
    const currentDate = Date.now();
    const data = new FormData();
    let newName;
    if (selectedFile) {
      newName = `${currentDate}-${selectedFile.name}`;
      data.append('file', selectedFile, `${newName}`);
      axios.post(`${REACT_APP_API_URL}/upload`, data).then((res) => {
        console.log(res.statusText);
      });
    }

    axios
      .patch(`${REACT_APP_API_URL}/activities/${id}`, {
        title: title,
        description: description,
        fileName: newName || coverImageName,
      })
      .then((res) => {
        setMessage(res.data.message);
        loadData();
      })
      .catch((err) => console.error(err));
  };

  const loadData = () => {
    axios
      .get(`${REACT_APP_API_URL}/activities/${id}`)
      .then((res) => {
        setDescription(res.data[0].description);
        setTitle(res.data[0].title);
        setCoverImageName(res.data[0].coverImageName);
        console.log(res.data[0].description);
      })
      .catch((err) => console.error(err));
  };

  return (
    <div className="main container admin-content">
      <div className="row">
        <div className="col-sm-12">
          <h1>Edit activity</h1>
          <Form className="mb-2">
            <Form.Group controlId="formActivityTitle" className="mb-3">
              <Form.Label>Activity title</Form.Label>
              <Form.Control
                onChange={(event) => changeTitleHandler(event)}
                type="text"
                placeholder="Activity title"
                value={title}
              />
            </Form.Group>
            <Form.Group controlId="formActivityDescription" className="mb-3">
              <Form.Label>Activity description</Form.Label>
              <Form.Control
                onChange={(event) => changeDescriptionHandler(event)}
                as="textarea"
                rows={3}
                value={description}
              />
            </Form.Group>
            <div className="img-thumbnail-wrap mb-3">
              <img
                src={`${process.env.PUBLIC_URL}/uploads/${coverImageName}`}
                alt="cover-img"
              />
            </div>
            <Form.Group className="mb-3">
              <Form.File
                onChange={(event) => uploadFileHandler(event)}
                id="exampleFormControlFile1"
                label="Example file input"
              />
            </Form.Group>
            <Button
              variant="primary"
              type="submit"
              onClick={(event) => onClickHandler(event)}
            >
              Update
            </Button>
          </Form>
          {message ? <p>{message}</p> : ''}
        </div>
      </div>
    </div>
  );
};

export default EditActivity;
