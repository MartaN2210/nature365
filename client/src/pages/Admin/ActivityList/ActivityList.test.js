import React from 'react';
import { render, waitFor, cleanup } from '@testing-library/react';
import mockAxios from 'axios';
import { BrowserRouter as Router } from 'react-router-dom';
import ActivityList from './ActivityList';

afterEach(cleanup);
it('mocking axios request for the activity list', async () => {
  const data = {
    data: [
      {
        idActivity: 1,
        title: 'title 1',
      },
      {
        idActivity: 2,
        title: 'title 2',
      },
      {
        idActivity: 3,
        title: 'mocked title',
      },
    ],
  };
  mockAxios.get.mockResolvedValueOnce(data);
  const { getByText } = render(
    <Router>
      <ActivityList />
    </Router>
  );
  await waitFor(() => {
    expect(getByText('mocked title'));
  });
});
