import React, { useState, useEffect } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import axios from 'axios';
import { AiOutlineDelete } from 'react-icons/ai';
import { FiEdit } from 'react-icons/fi';
import { Link } from 'react-router-dom';

import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';
import Loader from 'react-loader-spinner';

export default function ActivityList() {
  const { REACT_APP_API_URL } = process.env;
  const [isLoading, setIsLoading] = useState(true);
  const [activities, setActivities] = useState([]);

  useEffect(() => {
    loadData();
  }, []);

  const loadData = async () => {
    axios
      .get(`${REACT_APP_API_URL}/activities`)
      .then((res) => {
        setActivities(res.data);
        setIsLoading(false);
      })
      .catch((err) => console.error(err));
  };

  const deleteHandler = (idActivity) => {
    axios
      .delete(`${REACT_APP_API_URL}/activities/${idActivity}`)
      .then((res) => loadData())
      .catch((err) => console.error(err));
  };

  return (
    <div className="main container admin-content">
      <div className="row">
        <div className="col-sm-12">
          <h1>Activities</h1>
          {isLoading ? (
            <Loader type="Oval" color="#00BFFF" />
          ) : (
            <Container className="table">
              <Row>
                <Col xs={2}>Added date</Col>
                <Col xs={2}>Title</Col>
                <Col xs={4}>Description</Col>
                <Col xs={2}>Image</Col>
                <Col xs={1} />
                <Col xs={1} />
              </Row>
              {activities.map((activity) => (
                <Row key={activity.idActivity} className="align-items-center">
                  <Col xs={2}>{activity.addDate}</Col>
                  <Col xs={2} className="activity-title">
                    {activity.title}
                  </Col>
                  <Col xs={4}>{activity.description}</Col>
                  <Col xs={2}>
                    <div className="img-thumbnail-list-wrap">
                      <img
                        src={`${process.env.PUBLIC_URL}/uploads/${activity.coverImageName}`}
                        alt="cover-img"
                      />
                    </div>
                  </Col>
                  <Col xs={1}>
                    <Link to={`/editactivity/${activity.idActivity}`}>
                      <FiEdit color="green" size={24} />
                    </Link>
                  </Col>
                  <Col xs={1}>
                    <AiOutlineDelete
                      className="deleteButton"
                      type="button"
                      onClick={() => deleteHandler(activity.idActivity)}
                      color="red"
                      size={30}
                    />
                  </Col>
                </Row>
              ))}
            </Container>
          )}
        </div>
      </div>
    </div>
  );
}
