import React, { useState } from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';

export default function HomeAdmin() {
  const { REACT_APP_API_URL } = process.env;
  const [email, setEmail] = useState('');
  const [message, setMessage] = useState();

  const handleSubmit = (event) => {
    event.preventDefault();
    const requestOptions = {
      method: 'POST',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
      body: JSON.stringify({
        email: email,
      }),
    };

    // create user:
    (async () => {
      try {
        const response = await fetch(
          `https://${REACT_APP_API_URL}/auth/add_user`,
          requestOptions
        );
        if (!response.ok) {
          if (response.status === 400 || response.status === 429) {
            const data = await response.json();
            setMessage(data.message);
          } else {
            throw new Error(`HTTP error! status: ${response.status}`);
          }
        }
      } catch (error) {
        console.error(error);
        setMessage('Ups! Something went wrong. Please try again');
      }
    })();
  };

  return (
    <Form onSubmit={handleSubmit}>
      <FormGroup>
        <h3>Generate sign up link for new user</h3>
        <Label for="email">Email</Label>
        <Input
          data-testid="email"
          type="email"
          name="email"
          id="email"
          placeholder="write your email"
          onChange={(event) => setEmail(event.target.value)}
        />
        {message ? <p>{message}</p> : ''}
      </FormGroup>
      <Button>Submit</Button>
    </Form>
  );
}
