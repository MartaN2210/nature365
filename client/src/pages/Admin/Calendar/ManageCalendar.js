import React, { useState, useEffect } from 'react';
import Calendar from 'react-calendar';
import { Form, Container, Row, Col, Button } from 'react-bootstrap';
import axios from 'axios';
import './manageCalendar.scss';

export default function ManageCalendar() {
  const { REACT_APP_API_URL } = process.env;
  const [date, setDate] = useState(new Date());
  const [message, setMessage] = useState();
  const [stories, setStories] = useState();
  const [activities, setActivities] = useState();
  const [dayId, setDayId] = useState();
  const [selectedActivity, setSelectedActivity] = useState({
    label: 'Select an activity',
    value: undefined,
  });
  const [selectedStory, setSelectedStory] = useState({
    label: 'Select a story',
    value: undefined,
  });
  const [introText, setIntroText] = useState('');
  const [selected, setSelected] = useState(true);

  useEffect(() => {
    loadData();
    setSelected(true);
  }, [date]);

  const loadData = async () => {
    const day = date.getDate();
    const month = date.getMonth() + 1;

    axios
      .get(`${REACT_APP_API_URL}/days/${month}/${day}`)
      .then((res) => {
        if (res.data[0] && res.data[0].idCalendarDay) {
          setDayId(res.data[0].idCalendarDay);
        } else {
          setDayId(undefined);
        }
        if (res.data[0] && res.data[0].storyTitle) {
          setSelectedStory({
            label: res.data[0].storyTitle,
            value: res.data[0].idStory,
          });
        } else {
          setSelectedStory({
            label: 'Select a story',
            value: undefined,
          });
        }
        if (res.data[0] && res.data[0].activityTitle) {
          setSelectedActivity({
            label: res.data[0].activityTitle,
            value: res.data[0].idActivity,
          });
        } else {
          setSelectedActivity({
            label: 'Select an activity',
            value: undefined,
          });
        }
        if (res.data[0] && res.data[0].introText) {
          setIntroText(res.data[0].introText);
        } else {
          setIntroText('');
        }
      })
      .catch((err) => console.error(err));
    axios
      .get(`${REACT_APP_API_URL}/stories`)
      .then((res) => setStories(res.data))
      .catch((err) => console.error(err));
    axios
      .get(`${REACT_APP_API_URL}/activities`)
      .then((res) => setActivities(res.data))
      .catch((err) => console.error(err));
  };

  const onChange = (data) => {
    setMessage('');
    setDate(data);
    loadData();
  };

  const handleChooseActivity = (label, value) => {
    setSelected(false);
    setSelectedActivity({
      label: label,
      value: value,
    });
  };
  const handleChooseStory = (label, value) => {
    setSelected(false);
    setSelectedStory({
      label: label,
      value: value,
    });
  };
  const handleProvideIntroText = (value) => {
    setIntroText(value);
  };

  const updateHandler = () => {
    const day = date.getDate();
    const month = date.getMonth() + 1;
    console.log(dayId);
    if (!dayId) {
      axios
        .post(`${REACT_APP_API_URL}/days`, {
          idStory: selectedStory.value || undefined,
          idActivity: selectedActivity.value || undefined,
          day: day,
          month: month,
          introText: introText || '',
        })
        .then((res) => setMessage(res.data.message))
        .catch((err) => console.error(err));
    } else {
      axios
        .patch(`${REACT_APP_API_URL}/days/${dayId}`, {
          idStory: selectedStory.value || undefined,
          idActivity: selectedActivity.value || undefined,
          introText: introText || '',
        })
        .then((res) => setMessage(res.data.message))
        .catch((err) => console.error(err));
    }
  };

  return (
    <Container className="manage-calendar-content">
      <Row className="mb-5 justify-content-center">
        <Col xs={6} className="d-flex justify-content-center">
          <Calendar onChange={onChange} value={date} />
        </Col>

        <Col xs={4} className="info-container p-5">
          <Form>
            <Container>
              <h3>
                {` ${date.getDate()}. ${date.toLocaleString('default', {
                  month: 'long',
                })}`}
              </h3>
              <Row>
                <Col xs={12}>
                  <Form.Group controlId="stories">
                    <Form.Label>Select a story</Form.Label>
                    <Form.Control
                      as="select"
                      name="stories"
                      onChange={(event) => {
                        handleChooseStory(
                          event.target[event.target.selectedIndex].getAttribute(
                            'data-label'
                          ),
                          Number(event.target.value)
                        );
                      }}
                    >
                      <option selected={selected} value={selectedStory.value}>
                        {selectedStory.label}
                      </option>
                      {stories ? (
                        stories.map((story) => (
                          <option
                            key={story.idStory}
                            data-label={story.title}
                            value={story.idStory}
                          >
                            {story.title}
                          </option>
                        ))
                      ) : (
                        <option>is loading</option>
                      )}
                    </Form.Control>
                  </Form.Group>
                </Col>
                <Col xs={12}>
                  <Form.Group controlId="activities">
                    <Form.Label>Select an activity</Form.Label>
                    <Form.Control
                      as="select"
                      onChange={(event) => {
                        handleChooseActivity(
                          event.target[event.target.selectedIndex].getAttribute(
                            'data-label'
                          ),
                          Number(event.target.value)
                        );
                      }}
                      name="activities"
                    >
                      <option
                        selected={selected}
                        value={selectedActivity.value}
                      >
                        {selectedActivity.label}
                      </option>
                      {activities ? (
                        activities.map((activity) => (
                          <option
                            key={activity.idActivity}
                            data-label={activity.title}
                            value={activity.idActivity}
                          >
                            {activity.title}
                          </option>
                        ))
                      ) : (
                        <option>is loading</option>
                      )}
                    </Form.Control>
                  </Form.Group>
                </Col>
                <Col xs={12}>
                  <Form.Group controlId="Textarea1">
                    <Form.Label>Introduction text</Form.Label>
                    <Form.Control
                      as="textarea"
                      rows={6}
                      value={introText || ''}
                      onChange={(event) => {
                        handleProvideIntroText(event.target.value);
                      }}
                    />
                  </Form.Group>
                </Col>
                <Col xs={12}>
                  {message ? (
                    <p className="validation-message">{message}</p>
                  ) : (
                    ''
                  )}
                  <Button type="button" onClick={updateHandler}>
                    Update
                  </Button>
                </Col>
              </Row>
            </Container>
          </Form>
        </Col>
      </Row>
    </Container>
  );
}
