import React, { useState } from 'react';
import { Form, Button } from 'react-bootstrap';
import axios from 'axios';

const AddStory = () => {
  const { REACT_APP_API_URL } = process.env;
  const [selectedFile, setSelectedFile] = useState(null);
  const [title, setTitle] = useState(null);
  const [excerpt, setExcerpt] = useState(null);
  const [message, setMessage] = useState();

  const uploadFileHandler = (event) => {
    setSelectedFile(event.target.files[0]);
  };

  const changeTitleHandler = (event) => {
    setTitle(event.target.value);
  };

  const changeExcerptHandler = (event) => {
    setExcerpt(event.target.value);
  };

  const onClickHandler = (event) => {
    event.preventDefault();
    const currentDate = Date.now();
    const data = new FormData();
    let newName;
    if (selectedFile) {
      newName = `${currentDate}-${selectedFile.name}`;
      data.append('file', selectedFile, `${newName}`);
      axios.post(`${REACT_APP_API_URL}/upload`, data).then((res) => {
        console.log(res.statusText);
      });
    }

    axios
      .post(`${REACT_APP_API_URL}/stories`, {
        title: title,
        excerpt: excerpt,
        fileName: newName || '',
      })
      .then((res) => setMessage(res.data.message))
      .catch((err) => console.error(err));
  };
  return (
    <div className="main container admin-content">
      <div className="row">
        <div className="col-sm-12">
          <h1>Add story</h1>
          <Form>
            <Form.Group controlId="formStoryTitle" className="mb-3">
              <Form.Label>Story title</Form.Label>
              <Form.Control
                onChange={(event) => changeTitleHandler(event)}
                type="text"
                placeholder="Story title"
              />
            </Form.Group>
            <Form.Group controlId="formStoryExcerpt" className="mb-3">
              <Form.Label>Story excerpt</Form.Label>
              <Form.Control
                onChange={(event) => changeExcerptHandler(event)}
                as="textarea"
                rows={3}
              />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.File
                onChange={(event) => uploadFileHandler(event)}
                id="exampleFormControlFile1"
                label="Example file input"
              />
            </Form.Group>
            <Button
              variant="primary"
              type="submit"
              onClick={(event) => onClickHandler(event)}
            >
              Publish
            </Button>
          </Form>
          {message ? <p>{message}</p> : ''}
        </div>
      </div>
    </div>
  );
};

export default AddStory;
