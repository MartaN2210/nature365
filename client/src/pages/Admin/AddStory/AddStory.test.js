import React from 'react';
import { render, cleanup } from '@testing-library/react';
import ReactDOM from 'react-dom';
import AddStory from './AddStory';

// test('renders learn react link', () => {
//   const { getByText } = render(<App />);
//   const linkElement = getByText(/learn react/i);
//   expect(linkElement).toBeInTheDocument();
// });

afterEach(cleanup);

it('renders add story without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<AddStory />, div);
});
