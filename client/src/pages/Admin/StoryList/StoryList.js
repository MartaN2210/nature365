import React, { useState, useEffect } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import axios from 'axios';
import { AiOutlineDelete } from 'react-icons/ai';
import { FiEdit } from 'react-icons/fi';
import 'react-loader-spinner/dist/loader/css/react-spinner-loader.css';
import Loader from 'react-loader-spinner';
import { Link } from 'react-router-dom';

export default function StoryList() {
  const { REACT_APP_API_URL } = process.env;
  const [isLoading, setIsLoading] = useState(true);
  const [stories, setStories] = useState([]);

  useEffect(() => {
    setIsLoading(false);
    loadData();
  }, []);

  const loadData = async () => {
    console.log('test');
    axios
      .get(`${REACT_APP_API_URL}/stories`)
      .then((res) => setStories(res.data))
      .catch((err) => console.error(err));
  };

  const deleteHandler = (idStory) => {
    axios
      .delete(`${REACT_APP_API_URL}/stories/${idStory}`)
      .then((res) => loadData())
      .catch((err) => console.error(err));
  };

  return (
    <div className="main container admin-content">
      <div className="row">
        <div className="col-sm-12">
          <h1>Stories</h1>
          {isLoading ? (
            <Loader type="Oval" color="#00BFFF" />
          ) : (
            <Container className="table">
              <Row>
                <Col xs={2}>Added date</Col>
                <Col xs={2}>Title</Col>
                <Col xs={4}>Excerpt</Col>
                <Col xs={2}>Image</Col>
                <Col xs={1} />
                <Col xs={1} />
              </Row>
              {stories.map((story) => (
                <Row key={story.idStory} className="align-items-center">
                  <Col xs={2}>{story.addDate}</Col>
                  <Col xs={2}>{story.title}</Col>
                  <Col xs={4}>{story.excerpt}</Col>
                  <Col xs={2}>
                    <div className="img-thumbnail-list-wrap">
                      <img
                        src={`${process.env.PUBLIC_URL}/uploads/${story.coverImageName}`}
                        alt="cover-img"
                      />
                    </div>
                  </Col>
                  <Col xs={1}>
                    <Link to={`/editstory/${story.idStory}`}>
                      <FiEdit color="green" size={24} />
                    </Link>
                  </Col>
                  <Col xs={1}>
                    <AiOutlineDelete
                      className="deleteButton"
                      type="button"
                      onClick={() => deleteHandler(story.idStory)}
                      color="red"
                      size={30}
                    />
                  </Col>
                </Row>
              ))}
            </Container>
          )}
        </div>
      </div>
    </div>
  );
}
