import React, { useState } from 'react';
import { Form, Button } from 'react-bootstrap';
import axios from 'axios';

const AddActivity = () => {
  const { REACT_APP_API_URL } = process.env;
  const [selectedFile, setSelectedFile] = useState(null);
  const [title, setTitle] = useState(null);
  const [description, setDescription] = useState(null);
  const [message, setMessage] = useState();

  const uploadFileHandler = (event) => {
    setSelectedFile(event.target.files[0]);
  };

  const changeTitleHandler = (event) => {
    setTitle(event.target.value);
  };

  const changeDescriptionHandler = (event) => {
    setDescription(event.target.value);
  };

  const onClickHandler = (event) => {
    event.preventDefault();
    const currentDate = Date.now();
    const data = new FormData();
    let newName;
    if (selectedFile) {
      newName = `${currentDate}-${selectedFile.name}`;
      data.append('file', selectedFile, `${newName}`);
      axios.post(`${REACT_APP_API_URL}/upload`, data).then((res) => {
        console.log(res.statusText);
      });
    }

    axios
      .post(`${REACT_APP_API_URL}/activities`, {
        title: title,
        description: description,
        fileName: newName || '',
      })
      .then((res) => setMessage(res.data.message))
      .catch((err) => console.error(err));
  };
  return (
    <div className="main container admin-content">
      <div className="row">
        <div className="col-sm-12">
          <h1>Add activity</h1>
          <Form>
            <Form.Group controlId="formActivityTitle" className="mb-3">
              <Form.Label>Activity title</Form.Label>
              <Form.Control
                onChange={(event) => changeTitleHandler(event)}
                type="text"
                placeholder="Activity title"
              />
            </Form.Group>
            <Form.Group controlId="formActivityDescription" className="mb-3">
              <Form.Label>Activity description</Form.Label>
              <Form.Control
                onChange={(event) => changeDescriptionHandler(event)}
                as="textarea"
                rows={3}
              />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.File
                onChange={(event) => uploadFileHandler(event)}
                id="exampleFormControlFile1"
                label="Example file input"
              />
            </Form.Group>
            <Button
              variant="primary"
              type="submit"
              onClick={(event) => onClickHandler(event)}
            >
              Publish
            </Button>
          </Form>
          {message ? <p>{message}</p> : ''}
        </div>
      </div>
    </div>
  );
};

export default AddActivity;
