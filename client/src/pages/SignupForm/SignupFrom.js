import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Form, Button, Container, Row, Col } from 'react-bootstrap';
import img from '../../assets/images/loginimg.png';

export default function SignupForm(props) {
  const { REACT_APP_API_URL } = process.env;
  const [name, setName] = useState();
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();
  const [message, setMessage] = useState();

  const history = useHistory();

  const handleInputNameChanged = (event) => {
    setName(event.target.value);
    if (event.target.value.length < 3) {
      setMessage('The name should have at least 3 characters');
    } else {
      setMessage('');
    }
  };
  const handleInputEmailChanged = (event) => {
    setEmail(event.target.value);
    if (!validateEmail(event.target.value)) {
      setMessage('Please provide the valid email address');
    } else {
      setMessage('');
    }
  };

  const handleInputPasswordChanged = (event) => {
    setPassword(event.target.value);
  };

  const handleFormSubmit = (event) => {
    event.preventDefault();
    if (!validateEmail(event.target.value) || event.target.value.length < 3) {
      setMessage('Please provide provide correct data');
    } else {
      createUser();
    }
  };

  const createUser = () => {
    const requestOptions = {
      method: 'POST',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
      body: JSON.stringify({
        name: name,
        email: email,
        password: password,
      }),
    };

    (async () => {
      try {
        const response = await fetch(
          `${REACT_APP_API_URL}/auth/signup`,
          requestOptions
        );
        if (!response.ok) {
          if (response.status === 400 || response.status === 429) {
            const data = await response.json();
            setMessage(data.message);
          } else {
            throw new Error(`HTTP error! status: ${response.status}`);
          }
        } else {
          history.push('/');
        }
      } catch (error) {
        console.error(error);
        setMessage('Ups! Something went wrong. Please try again');
      }
    })();
  };
  const validateEmail = (emailValue) => {
    const re =
      /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(emailValue);
  };

  return (
    <Container className="login-content shadow p-0">
      <Row>
        <Col
          xs={12}
          md={6}
          className="d-flex justify-content-center align-items-center"
        >
          <img className="cover-img" src={img} alt="img-cover" />
        </Col>
        <Col
          xs={12}
          md={6}
          className="d-flex flex-column justify-content-center align-items-center p-5 pt-1 pt-md-5"
        >
          <h2 className="w-100"> Sign up</h2>
          <Form className="w-100" onSubmit={handleFormSubmit}>
            <Form.Group controlId="formBasicName" className="mb-2">
              <Form.Label>Whats your name?</Form.Label>
              <Form.Control
                type="text"
                placeholder="name"
                onChange={handleInputNameChanged}
              />
            </Form.Group>
            <Form.Group controlId="formBasicEmail" className="mb-2">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                type="email"
                placeholder="Enter email"
                onChange={handleInputEmailChanged}
              />
            </Form.Group>

            <Form.Group controlId="formBasicPassword" className="mb-2">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                onChange={handleInputPasswordChanged}
                required
              />
            </Form.Group>
            {message ? <p style={{ color: 'red' }}>{message}</p> : ''}
            <Button variant="primary" type="submit">
              Sign up
            </Button>
          </Form>
          <a className="w-100" href="/login">
            log in
          </a>
        </Col>
      </Row>
    </Container>
  );
}
