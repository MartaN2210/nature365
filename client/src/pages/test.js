function test(a, b) {
  return [a + b, b - a];
}

const [sum, subtrack] = test(2, 5);

const a = {
  number1: 1,
  number2: 2,
};
a.number3 = 7;

const { number1 } = a;

const arr = [1, 2, 3, 4];

console.log(number1);

const arr2 = arr.map((item) => item + 6);

console.log(arr2);
