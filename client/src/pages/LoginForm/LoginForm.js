import React, { useState, useContext } from 'react';
import { useHistory } from 'react-router-dom';
import { Form, Button, Container, Row, Col } from 'react-bootstrap';
import { BsCardImage } from 'react-icons/bs';
import axios from 'axios';
import { AuthApi } from '../../context/AuthApi';
import img from '../../assets/images/loginimg.png';
import './loginForm.scss';

function LoginForm(props) {
  const { REACT_APP_API_URL } = process.env;
  const [email, setEmail] = useState();
  const [password, setPassword] = useState();
  const [message, setMessage] = useState();
  const [auth, setAuth] = useContext(AuthApi);

  const history = useHistory();

  const handleInputEmailChanged = (event) => {
    setEmail(event.target.value);
    if (!validateEmail(event.target.value)) {
      setMessage('Please provide the valid email address');
    } else {
      setMessage('');
    }
  };

  const handleInputPasswordChanged = (event) => {
    setPassword(event.target.value);
  };

  const handleFormSubmit = (event) => {
    axios.defaults.withCredentials = true;
    event.preventDefault();
    const loginBody = {
      email: email,
      password: password,
    };
    const loginConfig = {
      headers: { crossDomain: true, 'Content-Type': 'application/json' },
      withCredentials: true,
    };

    if (!validateEmail(email)) {
      setMessage('Please provide the valid email address');
    } else {
      axios
        .post(`${REACT_APP_API_URL}/auth/login`, loginBody, loginConfig)
        .then((res) => {
          setAuth(res.data);
          if (res.data.userRoleId === 1) {
            history.push('/calendar');
          } else {
            history.push('/');
          }
        })
        .catch((err) => {
          console.log(err);
          setMessage(err.response.data.message);
        });
    }
  };

  const validateEmail = (emailValue) => {
    const re =
      /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(emailValue);
  };

  return (
    <Container className="login-content shadow p-0">
      <Row>
        <Col
          xs={12}
          md={6}
          className="d-flex justify-content-center align-items-center"
        >
          <img className="cover-img" src={img} alt="img-cover" />
        </Col>
        <Col
          xs={12}
          md={6}
          className="d-flex flex-column justify-content-center align-items-center px-5 py-5"
        >
          <h2 className="w-100">Welcome back</h2>
          <Form className="w-100" onSubmit={handleFormSubmit}>
            <Form.Group controlId="formBasicEmail" className="mb-2">
              <Form.Label>Email address</Form.Label>
              <Form.Control
                placeholder="Enter email"
                onChange={handleInputEmailChanged}
                data-testid="email"
              />
            </Form.Group>

            <Form.Group controlId="formBasicPassword" className="mb-2">
              <Form.Label>Password</Form.Label>
              <Form.Control
                type="password"
                placeholder="Password"
                onChange={handleInputPasswordChanged}
                required
              />
            </Form.Group>
            {message ? <p style={{ color: 'red' }}>{message}</p> : ''}
            <Button variant="primary" type="submit">
              Login
            </Button>
          </Form>
          <div className="w-100 mt-2">
            <p className="text-left mb-0">Not a user?</p>
            <a className="w-100" href="/signup">
              sign up
            </a>
          </div>
        </Col>
      </Row>
    </Container>
  );
}

export default LoginForm;
