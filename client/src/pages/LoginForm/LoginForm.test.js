import * as React from 'react';
import { render, fireEvent, act } from '@testing-library/react';
import mockAxios from 'axios';
import LoginForm from './LoginForm';
import { AuthApi, AuthProvider } from '../../context/AuthApi';

it('renders the login form', () => {
  const { getByLabelText } = render(
    <AuthProvider>
      <LoginForm />
    </AuthProvider>
  );

  expect(getByLabelText('Email address')).not.toBeNull();
  expect(getByLabelText('Password')).not.toBeNull();
});

it('checks if validation message is on the page', async () => {
  const { getByText, getByTestId } = render(
    <AuthProvider>
      <LoginForm />
    </AuthProvider>
  );

  await act(async () => {
    fireEvent.change(getByTestId('email'), {
      target: { value: 'aaa' },
    });
  });
  expect(getByText('Please provide the valid email address'));
});
