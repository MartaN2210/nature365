import React, { useEffect, useState } from 'react';
import axios from 'axios';

export const DayContentApi = React.createContext();

export const DayContentProvider = (props) => {
  const [dayContent, setDayContent] = useState(false);

  useEffect(() => {
    loadData();
  }, []);

  const loadData = async () => {
    const { REACT_APP_API_URL } = process.env;
    const date = new Date();
    const day = date.getDate();
    const month = date.getMonth() + 1;
    axios
      .get(`${REACT_APP_API_URL}/days/${month}/${day}`)
      .then((res) => {
        setDayContent(res.data[0]);
      })
      .catch((err) => console.error(err));
  };

  return (
    <DayContentApi.Provider value={[dayContent, setDayContent]}>
      {props.children}
    </DayContentApi.Provider>
  );
};
