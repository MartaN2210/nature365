import React, { useState } from 'react';

export const NavApi = React.createContext();

export const NavProvider = (props) => {
  const [openNav, setOpenNav] = useState(false);

  return (
    <NavApi.Provider value={[openNav, setOpenNav]}>
      {props.children}
    </NavApi.Provider>
  );
};
