import React, { useEffect, useState } from 'react';

export const AuthApi = React.createContext();

export const AuthProvider = (props) => {
  const { REACT_APP_API_URL } = process.env;
  const [auth, setAuth] = useState(false);

  useEffect(() => {
    fetch(`${REACT_APP_API_URL}/auth/is_auth`)
      .then((res) => res.json())
      .then((data) => {
        console.log('i am executed');
        console.log(data);
        setAuth(data);
      });
  }, []);

  return (
    <AuthApi.Provider value={[auth, setAuth]}>
      {props.children}
    </AuthApi.Provider>
  );
};
