import React, { useContext, useEffect, useState } from 'react';
import './progressLine.scss';
import axios from 'axios';
import { motion } from 'framer-motion';
import { AuthApi } from '../../context/AuthApi';

const ProgressLine = ({ isProfilePage }) => {
  const { REACT_APP_API_URL } = process.env;
  const [auth] = useContext(AuthApi);
  const [accomplishedActivitiesLength, setAccomplishedActivitiesLength] =
    useState();
  const listContent = [
    'step-1',
    'step-2',
    'step-3',
    'step-4',
    'step-5',
    'step-6',
    'step-7',
    'step-8',
  ];
  const variants = () => ({
    showBackground: { background: ['#fff', '#f29949'] },
    hideBackground: { background: 'transparent' },
  });

  useEffect(() => {
    loadData();
  }, []);

  const loadData = async () => {
    axios
      .get(
        `${REACT_APP_API_URL}/accomplishedactivities/filter?idUser=${
          auth ? auth.idUser : ''
        }`
      )
      .then((res) => {
        console.log('length', res.data.length);
        setAccomplishedActivitiesLength(res.data.length);
        if (res.data.length === 8 && !isProfilePage) {
          addReward();
        }
        console.log(isProfilePage);
      })
      .catch((err) => console.error(err));
  };

  const addReward = () => {
    axios
      .post(`${REACT_APP_API_URL}/gainedrewards`, {
        idUser: auth.idUser,
        idReward: 1,
      })
      .then((res) => {
        console.log('done');
      })
      .catch((err) => console.error(err));
  };
  return (
    <div data-testid="progress-line" className="progress-line">
      {listContent.map((item, index) => (
        <motion.div
          className="dot"
          key={item}
          animate={
            index <= accomplishedActivitiesLength - 1
              ? 'showBackground'
              : 'hideBackground'
          }
          variants={variants()}
          transition={{ duration: 3, delay: index === 0 ? 1 : index + 1 }}
        />
      ))}
    </div>
  );
};

export default ProgressLine;
