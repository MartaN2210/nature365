import React from 'react';
import { cleanup, render } from '@testing-library/react';
import ProgressLine from './ProgressLine';

afterEach(cleanup);

it('renders Progress line without crashing', () => {
  const { getByTestId } = render(<ProgressLine />);

  expect(getByTestId('progress-line')).not.toBeNull();
});
