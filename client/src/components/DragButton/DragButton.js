import React, { useState, useContext, useEffect } from 'react';
import { motion, useMotionValue, useTransform, Position } from 'framer-motion';
import { useHistory } from 'react-router-dom';
import './dragButton.scss';
import axios from 'axios';
import { DayContentApi } from '../../context/DayContentApi';
import { AuthApi } from '../../context/AuthApi';

const DragButton = () => {
  const { REACT_APP_API_URL } = process.env;
  const [auth] = useContext(AuthApi);
  const [dayContent] = useContext(DayContentApi);
  const [initialPosition, setInitialPosition] = useState();
  const [accomplished, setAccomplished] = useState(false);
  const history = useHistory();
  const x = useMotionValue(0);
  const xInput = [0, 100];
  const background = useTransform(x, xInput, ['#F29949', '#D65059']);
  const color = useTransform(x, xInput, ['#F29949', '#D65059']);
  const tickPath = useTransform(x, [10, 100], [0, 1]);

  useEffect(() => {
    loadData();
  }, [dayContent]);

  const loadData = async () => {
    axios
      .get(
        `${REACT_APP_API_URL}/accomplishedactivities/${
          dayContent ? dayContent.idActivity : '2'
        }/${auth ? auth.idUser : ''}`
      )
      .then((res) => {
        console.log(res.data);
        if (res.data.length > 0) {
          setAccomplished(true);
        }
      })
      .catch((err) => console.error(err));
  };

  const handleDragStart = (event, pointX) => {
    console.log(dayContent);
    setInitialPosition(pointX);
  };
  const handleDragEnd = (event, pointX) => {
    if (pointX - initialPosition > 100) {
      axios
        .post(`${REACT_APP_API_URL}/accomplishedactivities`, {
          idUser: auth.idUser,
          idActivity: dayContent ? dayContent.idActivity : '2',
        })
        .then((res) => {
          history.push('/progress');
          console.log('done');
        })
        .catch((err) => console.error(err));
    }
  };
  if (!accomplished) {
    return (
      <div>
        <motion.div className="drag-button-container" style={{ background }}>
          <motion.button
            className="box"
            style={{ x }}
            drag="x"
            dragConstraints={{ left: 0, right: 150 }}
            dragElastic={0}
            dragMomentum={false}
            onDragStart={(event, info) => handleDragStart(event, info.point.x)}
            onDragEnd={(event, info) => handleDragEnd(event, info.point.x)}
          >
            <svg className="progress-icon" viewBox="0 0 50 50">
              <motion.path
                fill="none"
                strokeWidth="2"
                stroke={color}
                d="M 0, 20 a 20, 20 0 1,0 40,0 a 20, 20 0 1,0 -40,0"
                style={{ translateX: 5, translateY: 5 }}
              />
              <motion.path
                fill="none"
                strokeWidth="2"
                stroke={color}
                d="M14,26 L 22,33 L 35,16"
                strokeDasharray="0 1"
                style={{ pathLength: tickPath }}
              />
            </svg>
          </motion.button>
        </motion.div>
        <p className="drag-button-text"> drag to mark as done</p>
      </div>
    );
  }
  return <p> The activity has been marked as done</p>;
};

export default DragButton;
