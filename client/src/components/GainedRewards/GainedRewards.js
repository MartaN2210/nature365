import React, { useState, useEffect, useContext } from 'react';
import axios from 'axios';
import { AuthApi } from '../../context/AuthApi';
import './gainedRewards.scss';

const GainedRewards = () => {
  const { REACT_APP_API_URL } = process.env;
  const [auth] = useContext(AuthApi);
  const [gainedRewards, setGainedRewards] = useState();
  useEffect(() => {
    loadData();
  }, []);

  const loadData = async () => {
    axios
      .get(
        `${REACT_APP_API_URL}/gainedrewards/filter?idUser=${
          auth ? auth.idUser : ''
        }`
      )
      .then((res) => {
        console.log(res.data);
        setGainedRewards(res.data);
      })
      .catch((err) => console.error(err));
  };
  return (
    <>
      {gainedRewards
        ? gainedRewards.map((reward) => (
            <img
              className="reward-img"
              src={`${process.env.PUBLIC_URL}/uploads/${reward.coverImageName}`}
              alt={reward.coverImageName}
            />
          ))
        : ''}
    </>
  );
};

export default GainedRewards;
