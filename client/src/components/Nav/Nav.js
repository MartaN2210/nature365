import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import { FaBookReader, FaChild, FaUserAlt } from 'react-icons/fa';
import { BsCalendar, BsFillStarFill } from 'react-icons/bs';
import { FiLogOut } from 'react-icons/fi';
import { motion } from 'framer-motion';
import { AuthApi } from '../../context/AuthApi';
import './nav.scss';

const Nav = ({ showNav, showNavHandler }) => {
  const [auth] = useContext(AuthApi);

  const variants = () => ({
    show: { opacity: 1 },
    hide: { opacity: 0 },
  });

  const closeHandler = () => {
    showNavHandler();
  };

  if (auth.userRoleId === 1) {
    return (
      <nav>
        <ul>
          <Link to="/addstory">Add story</Link>
        </ul>
        <ul>
          <Link to="/addactivity">Add activity</Link>
        </ul>
        <ul>
          <Link to="/calendar">Calendar</Link>
        </ul>
        <ul>
          <Link to="/list">Users</Link>
        </ul>
        <ul>
          <Link to="/stories">Stories</Link>
        </ul>
        <ul>
          <Link to="/activities">Activities</Link>
        </ul>
        <ul>
          <Link to="/logout">Logout</Link>
        </ul>
      </nav>
    );
  }
  return (
    <nav className={`userNav ${showNav ? 'showFlex' : ''}`}>
      <ul className="d-flex flex-column justify-content-around">
        <motion.li
          animate={showNav === false ? 'hide' : 'show'}
          variants={variants()}
          transition={{ duration: showNav === false ? 0 : 1, delay: 0 }}
        >
          <BsCalendar color="#fffffc" size={25} />
          <Link onClick={closeHandler} className="ml-3" to="/">
            Today
          </Link>
        </motion.li>
        <motion.li
          animate={showNav === false ? 'hide' : 'show'}
          variants={variants()}
          transition={{ duration: showNav === false ? 0 : 1, delay: 0.3 }}
        >
          <FaBookReader color="#fffffc" size={25} />
          <Link onClick={closeHandler} className="ml-3" to="/storyCover">
            Todays story
          </Link>
        </motion.li>
        <motion.li
          animate={showNav === false ? 'hide' : 'show'}
          variants={variants()}
          transition={{ duration: showNav === false ? 0 : 1, delay: 0.5 }}
        >
          <FaChild color="#fffffc" size={25} />
          <Link onClick={closeHandler} className="ml-3" to="/activity">
            Todays activity
          </Link>
        </motion.li>
        <motion.li
          animate={showNav === false ? 'hide' : 'show'}
          variants={variants()}
          transition={{ duration: showNav === false ? 0 : 1, delay: 0.5 }}
        >
          <FaUserAlt color="#fffffc" size={25} />
          <Link onClick={closeHandler} className="ml-3" to="/profile">
            Your profile
          </Link>
        </motion.li>
        <motion.li
          animate={showNav === false ? 'hide' : 'show'}
          variants={variants()}
          transition={{ duration: showNav === false ? 0 : 1, delay: 0.6 }}
        >
          <BsFillStarFill color="#fffffc" size={25} />
          <Link onClick={closeHandler} className="ml-3" to="/favouritestories">
            Favourite Stories
          </Link>
        </motion.li>
        <motion.li
          animate={showNav === false ? 'hide' : 'show'}
          variants={variants()}
          transition={{ duration: showNav === false ? 0 : 1, delay: 0.8 }}
        >
          <FiLogOut color="#fffffc" size={25} />
          <Link onClick={closeHandler} className="ml-3" to="/logout">
            Logout
          </Link>
        </motion.li>
      </ul>
    </nav>
  );
};

export default Nav;
