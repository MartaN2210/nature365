import React from 'react';
import { render, cleanup } from '@testing-library/react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import Nav from './Nav';
import { AuthApi, AuthProvider } from '../../context/AuthApi';

afterEach(cleanup);

it('renders Nav without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(
    <AuthProvider>
      <Router>
        <Nav />
      </Router>
    </AuthProvider>,
    div
  );
});
