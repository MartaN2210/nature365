import React, { useState, useContext, useEffect } from 'react';
import { BsFillStarFill } from 'react-icons/bs';
import axios from 'axios';
import { DayContentApi } from '../../context/DayContentApi';
import { AuthApi } from '../../context/AuthApi';

const FavStar = ({ idStory }) => {
  const { REACT_APP_API_URL } = process.env;
  const [dayContent] = useContext(DayContentApi);
  const [auth] = useContext(AuthApi);
  const [starColor, setStarColor] = useState('#fffffc');
  const [favourieStory, setFavouriteStory] = useState(false);

  useEffect(() => {
    loadData();
  }, [dayContent]);

  const loadData = async () => {
    axios
      .get(
        `${REACT_APP_API_URL}/favouritestories/${idStory}/${
          auth ? auth.idUser : ''
        }`
      )
      .then((res) => {
        console.log(res.data);
        if (res.data.length > 0) {
          console.log(res.data.length);
          setStarColor('#d65059');
          setFavouriteStory(true);
        }
      })
      .catch((err) => console.error(err));
  };

  const clickHandler = () => {
    console.log('test', favourieStory);
    if (!favourieStory) {
      axios
        .post(`${REACT_APP_API_URL}/favouritestories`, {
          idUser: auth.idUser,
          idStory: dayContent.idStory,
        })
        .then((res) => {
          setStarColor('#d65059');
          setFavouriteStory(true);
        })
        .catch((err) => console.error(err));
    } else {
      axios
        .delete(
          `${REACT_APP_API_URL}/favouritestories/${idStory}/${auth.idUser}`
        )
        .then((res) => {
          setStarColor('#fff');
          setFavouriteStory(false);
        })
        .catch((err) => console.error(err));
    }
  };

  return (
    <BsFillStarFill
      className="add-to-fav-icon"
      color={starColor}
      size={35}
      onClick={clickHandler}
    />
  );
};

export default FavStar;
