import React, { useEffect, useContext } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { AuthApi } from '../../context/AuthApi';
import './logout.css';

const Logout = () => {
  const { REACT_APP_API_URL } = process.env;
  const [auth, setAuth] = useContext(AuthApi);

  useEffect(() => {
    (async () => {
      const response = await fetch(`${REACT_APP_API_URL}/auth/logout`);
      const data = await response.json();
      console.log(data);
      setAuth(data);
    })();
  }, []);

  return (
    <Container className="main-container logout-container">
      <Row>
        <Col>
          <strong>Bye bye</strong>
        </Col>
      </Row>
      <Row>
        <Col>
          <Link to="/login">Login</Link>
        </Col>
      </Row>
    </Container>
  );
};

export default Logout;
