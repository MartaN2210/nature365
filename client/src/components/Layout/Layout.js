import React, { useContext, useEffect, useState } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';
import { motion, AnimatePresence } from 'framer-motion';
import Header from '../Header/Header';
import LoginForm from '../../pages/LoginForm/LoginForm';
import Home from '../../pages/User/Home/Home';
import SignupForm from '../../pages/SignupForm/SignupFrom';
import Logout from '../Logout/Logout';
import HomeAdmin from '../../pages/Admin/HomeAdmin/HomeAdmin';
import StoryList from '../../pages/Admin/StoryList/StoryList';
import Story from '../../pages/User/Story/Story';
import Activity from '../../pages/User/Activity/Activity';
import FavouriteStories from '../../pages/User/FavouriteStories/FavouriteStories';
import { AuthApi } from '../../context/AuthApi';
import { NavApi } from '../../context/NavApi';
import AddStory from '../../pages/Admin/AddStory/AddStory';
import AddActivity from '../../pages/Admin/AddActivity/AddActivity';
import ActivityList from '../../pages/Admin/ActivityList/ActivityList';
import EditStory from '../../pages/Admin/EditStory/EditStory';
import EditActivity from '../../pages/Admin/EditActivity/EditActivity';
import ManageCalendar from '../../pages/Admin/Calendar/ManageCalendar';
import ProgressPage from '../../pages/User/ProgressPage/ProgressPage';
import FooterNav from '../FooterNav/FooterNav';
import UserProfile from '../../pages/User/UserProfile/UserProfile';
import StoryCover from '../../pages/User/StoryCover/StoryCover';

const Layout = () => {
  const [openNav, setOpenNav] = useContext(NavApi);
  const [auth, setAuth] = useContext(AuthApi);

  const variants = () => ({
    show: {
      opacity: [0, 0, 1],
      y: ['-100%', '0%', '0%'],
    },
    hide: {
      opacity: [1, 0, 0],
      y: ['0%', '0%', '-100%'],
    },
  });

  return (
    <Container
      fluid
      className={`main-container ${
        auth.userRoleId === 2 || !auth.userRoleId
          ? 'user-content'
          : 'admin-content'
      }`}
    >
      <Row>
        <Col
          className={`p-1 ${
            auth.userRoleId === 2 || !auth.userRoleId ? 'col-12' : 'col-2'
          }`}
        >
          <Header />
        </Col>
        <motion.div
          className={`main-content col-10 p-0 p-md-1 ${
            auth.userRoleId === 2 || !auth.userRoleId ? 'col-12' : 'col-10'
          }`}
          animate={openNav === false ? 'show' : 'hide'}
          variants={variants()}
          transition={{
            duration: 1.4,
            ease: 'easeInOut',
          }}
        >
          <Routes />
        </motion.div>
      </Row>
      {auth.userRoleId === 2 ? <FooterNav /> : ''}
    </Container>
  );
};

const Routes = () => {
  const [auth] = useContext(AuthApi);
  console.log('check here', auth);

  return (
    <Switch>
      {/* <ProtectedRoute auth={auth} exact path="/admin" component={HomeAdmin} /> */}
      <Route auth={auth} exact path="/admin" component={HomeAdmin} />
      <ProtectedRoute auth={auth} exact path="/" component={Home} />
      <ProtectedRoute auth={auth} path="/story/:id" component={Story} />
      <ProtectedRoute auth={auth} path="/storyCover/" component={StoryCover} />
      <ProtectedRoute
        auth={auth}
        path="/chosenStoryCover/:id"
        component={StoryCover}
      />
      <ProtectedRoute auth={auth} path="/activity" component={Activity} />
      <ProtectedRoute auth={auth} path="/progress" component={ProgressPage} />
      <ProtectedRoute auth={auth} path="/stories" component={StoryList} />
      <ProtectedRoute auth={auth} path="/activities" component={ActivityList} />
      <ProtectedRoute
        auth={auth}
        path="/favouritestories"
        component={FavouriteStories}
      />
      <Route auth={auth} path="/login" component={LoginForm} />
      <Route auth={auth} path="/signup" component={SignupForm} />
      <ProtectedRoute auth={auth} path="/logout" component={Logout} />
      <ProtectedRoute auth={auth} path="/addstory" component={AddStory} />
      <ProtectedRoute auth={auth} path="/calendar" component={ManageCalendar} />
      <ProtectedRoute auth={auth} path="/addactivity" component={AddActivity} />
      <ProtectedRoute auth={auth} path="/editstory/:id" component={EditStory} />
      <ProtectedRoute
        auth={auth}
        path="/editactivity/:id"
        component={EditActivity}
      />
      <ProtectedRoute auth={auth} path="/profile" component={UserProfile} />
      <Route
        path="/server-error"
        render={() => (
          <p>
            Oh no! Something bad happened. Please come back later when we fixed
            that problem. Thanks.
          </p>
        )}
      />
      <Route render={() => <h3>Page not found</h3>} />
    </Switch>
  );
};

const ProtectedRoute = ({ auth, component: Component, ...rest }) => {
  if (auth) {
    return (
      <Route
        {...rest}
        render={() =>
          !auth.userRoleId ? <Redirect to="/login" /> : <Component />
        }
      />
    );
  }
  return 'loading';
};
export default Layout;
