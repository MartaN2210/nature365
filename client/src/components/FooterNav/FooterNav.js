import React from 'react';
import { FaBookReader, FaChild } from 'react-icons/fa';
import { BsFillStarFill } from 'react-icons/bs';
import { Link } from 'react-router-dom';
import './footerNav.scss';

const FooterNav = () => (
  <nav className="user-nav-footer d-block d-md-none fixed-bottom h-auto">
    <ul className="d-flex justify-content-around">
      <li>
        <Link
          className="d-flex flex-column align-items-center"
          to="/storyCover"
        >
          <FaBookReader color="#fffffc" size={25} />
          <p className="m-0 small">Story</p>
        </Link>
      </li>
      <li>
        <Link className="d-flex flex-column align-items-center" to="/activity">
          <FaChild color="#fffffc" size={25} />
          <p className="m-0 small">Activity</p>
        </Link>
      </li>
      <li>
        <Link
          className="d-flex flex-column align-items-center"
          to="/favouritestories"
        >
          <BsFillStarFill color="#fffffc" size={25} />
          <p className="m-0 small">Favourites </p>
        </Link>
      </li>
    </ul>
  </nav>
);
export default FooterNav;
