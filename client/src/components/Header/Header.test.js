import React from 'react';
import { render, cleanup } from '@testing-library/react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import Header from './Header';
import { AuthApi, AuthProvider } from '../../context/AuthApi';
import { NavApi, NavProvider } from '../../context/NavApi';

afterEach(cleanup);

it('renders Home without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(
    <AuthProvider>
      <NavProvider>
        <Router>
          <Header />
        </Router>
      </NavProvider>
    </AuthProvider>,
    div
  );
});
