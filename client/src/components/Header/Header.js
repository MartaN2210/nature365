import React, { useContext, useState } from 'react';
import { Link } from 'react-router-dom';
import { FaUserAlt } from 'react-icons/fa';
import Nav from '../Nav/Nav';
import logo from '../../assets/images/nature365-logo.png';
import { AuthApi } from '../../context/AuthApi';
import { NavApi } from '../../context/NavApi';
import './header.scss';

export default function Header() {
  const [auth] = useContext(AuthApi);
  const [openNav, setOpenNav] = useContext(NavApi);

  const showNavHandler = () => {
    if (openNav === false) {
      setOpenNav(true);
    } else {
      setOpenNav(false);
    }
  };

  if (auth.userRoleId === 1) {
    return (
      <header className="headerAdmin">
        <div className="logoWrap">
          <div>
            <img src={logo} alt="logo" />
          </div>
        </div>
        <Nav />
      </header>
    );
  }
  return (
    <header className="headerUser">
      <div className="headerBaner">
        <div className="logoWrap">
          <Link to="/">
            <img src={logo} alt="logo" />
          </Link>
        </div>
        {auth.userRoleId === 2 ? (
          <>
            <button
              type="button"
              onClick={showNavHandler}
              className="burgerIcon"
            >
              <div className="iconBar" />
              <div className="iconBar" />
              <div className="iconBar" />
            </button>

            <div className="d-md-none d-flex align-items-center">
              <Link className="ml-3" to="/profile">
                <FaUserAlt color="#fff" size={30} />
              </Link>
            </div>
          </>
        ) : (
          ''
        )}
      </div>
      <Nav showNav={openNav} showNavHandler={showNavHandler} />
    </header>
  );
}
