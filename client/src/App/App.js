import React, { useContext } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import './App.css';
import Layout from '../components/Layout/Layout';
import { AuthApi, AuthProvider } from '../context/AuthApi';
import { NavApi, NavProvider } from '../context/NavApi';
import { DayContentApi, DayContentProvider } from '../context/DayContentApi';

import '../assets/scss/theme.scss';

function App() {
  return (
    <DayContentProvider>
      <AuthProvider>
        <NavProvider>
          <Router basename={process.env.PUBLIC_URL}>
            <Layout />
          </Router>
        </NavProvider>
      </AuthProvider>
    </DayContentProvider>
  );
}

export default App;
